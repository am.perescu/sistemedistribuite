package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"
)

func main() {
	
    clientCount := 0
    allClients := make(map[net.Conn]int)
    newConnections := make(chan net.Conn)
    deadConnections := make(chan net.Conn)

    server, err := net.Listen("tcp", "127.0.0.1:8080")

    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

		fmt.Println("Server-ul asteapta conexiuni...")
		go func() {
			for {
				conn, err := server.Accept()
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}

				newConnections <- conn
			}
		}()

		for {
			select {

			case conn := <-newConnections:
				log.Printf("Client %d conectat", clientCount)

				allClients[conn] = clientCount
				clientCount += 1

				go func(conn net.Conn, clientId int) {
					fmt.Printf("Server a primit request de la client %d\n", clientId)
					fmt.Println("Server proceseaza datele")

					timestamp := time.Now()
					fmt.Printf("Server trimite %s catre client\n", timestamp)
					conn.Write([]byte(timestamp.Format(time.RFC3339Nano) + "\n"))

					deadConnections <- conn
				}(conn, allClients[conn])

			case conn := <-deadConnections:
				log.Printf("Client %d deconectat", allClients[conn])
				delete(allClients, conn)
			}
		}
}


