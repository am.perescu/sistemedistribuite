package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"time"
)

func main() {

	  
		t_min := 9999999 * time.Nanosecond    // t_min for requests one-way between client - server 
    
	  conn, err := net.Dial("tcp", "127.0.0.1:8080")

	  if err != nil {
	      fmt.Println(err)
	      return
	  }

	  fmt.Printf("Client conectat\n")

	  request_time := time.Now()
	  result_bytes, _ := bufio.NewReader(conn).ReadBytes('\n')
	  response_time := time.Now()
	  result := strings.TrimSpace(string(result_bytes))

	  server_time, err := time.Parse(time.RFC3339Nano, result)
	  if err != nil {
	      fmt.Println("Eroare:  ", err)
	      return
	  }

	  for i := 0; i < 5 ; i++ {
        conn, _:= net.Dial("tcp", "127.0.0.1:8080")

        t_request := time.Now()
        bufio.NewReader(conn).ReadBytes('\n')
				t_response := time.Now()
        latency := t_response.Sub(t_request)
        fmt.Println(latency)
        if latency < t_min {
            t_min = latency
        }
    }

	  latency := response_time.Sub(request_time)
	  client_time := server_time.Add(latency/2)
	  accuracy := latency/2 - t_min
	  client_time_accuracy := server_time.Add(accuracy)

	  fmt.Printf("Server time: %s\n", server_time)
	  fmt.Printf("Latency: %s\n", latency)
	  fmt.Printf("Accuracy %s\n", accuracy)
	  fmt.Printf("Request time: %s\n", request_time)
	  fmt.Printf("Response time: %s\n", response_time)
	  fmt.Printf("Sync time: %s\n", client_time)
	  fmt.Printf("Sync time with accuracy: %s\n", client_time_accuracy)
	             
}


