/*
Nume: Perescu Antonela Madalina
Grupa: 452
Tema 1
*/

package main

import (
	"Tema1/config"
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"regexp"
	"strings"
)

var vowels = []string{"a", "e", "i", "o", "u"}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func isValid(word string) bool{
	isValidBool := false
	vowelsCount := 0
	for i:=0; i < len(word); i++ {
		if Contains(vowels, string([]rune(word)[i])) {
			if i % 2 == 0 {
				vowelsCount++
				isValidBool = true
			} else {
				isValidBool = false
			}
		}
	}

	if isValidBool && vowelsCount % 2 == 0 {
		isValidBool = true
	} else {
		isValidBool = false
	}

	return isValidBool
}

func main() {

	file := flag.String("c", "config.txt", "Path")
	address, n := config.ReadFile(*file)

	clientCount := 0
	allClients := make(map[net.Conn]int)
	newConnections := make(chan net.Conn)
	deadConnections := make(chan net.Conn)

	//listen on all interfaces
	server, err := net.Listen("tcp", address)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Waiting for connections...")
	go func() {
		for {
			conn, err := server.Accept()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		newConnections <- conn
		}
	}()

	for {

		select {

			case conn := <-newConnections:

			log.Printf("Client %d conectat", clientCount)

			allClients[conn] = clientCount
			clientCount += 1

			go func(conn net.Conn, clientId int) {
				reader := bufio.NewReader(conn)
				for {
					incoming, err := reader.ReadString('\n')
					if err != nil {
						break
					}

					//look this
					re := regexp.MustCompile(`^[a-zA-Z0-9,]*$`)
					incoming = strings.TrimSuffix(incoming, "\n")
					re = regexp.MustCompile(`,+`)
					incoming = strings.Replace(incoming, " ", "", -1)
					incoming = re.ReplaceAllString(incoming, ",")
					words := strings.Split(incoming, ",")
					empty_word := strings.Replace(strings.Join(words, ""), ",", "", -1)
					if len(empty_word) < 1 {
						continue
					}

					fmt.Printf("Server a primit request cu datele %s de la clientul %d\n", incoming, clientId)
					var array_words []string
					for _, word := range words {
						array_words = append(array_words, word)
					}
					if len(array_words) == n {
						fmt.Println("Server proceseaza datele")
						var valid_words []string

						for i := 0; i < len(array_words); i++ {
							if isValid(array_words[i]) {
								valid_words = append(valid_words, array_words[i])
							}
						}
						outputMessage := fmt.Sprintf("Numarul de cuvinte este: %d \n", len(valid_words))

						fmt.Printf("Server trimite catre clientul %d,  mesajul: %s", allClients[conn], outputMessage)
						conn.Write([]byte(outputMessage))
					} else {
						outputMessage := fmt.Sprintf("Numarul de cuvinte ale array-ul trebuie sa fie egal cu: %d \n", n)
						conn.Write([]byte(outputMessage))
					}
				}

				deadConnections <- conn

			}(conn, allClients[conn])


			case conn := <-deadConnections:
				log.Printf("Client %d disconnected", allClients[conn])
				delete(allClients, conn)
		}
	}
}