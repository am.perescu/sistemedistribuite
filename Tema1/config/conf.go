package config

import (
	"io/ioutil"
	"strconv"
	"strings"
)

func ReadFile(file string) (string, int) {
	data, _ := ioutil.ReadFile(file)

	f := strings.Split(string(data), ",")
	address, num := f[0], f[1]
	n, _ := strconv.ParseUint(num, 10, 32)

	return address, int(n)
}
