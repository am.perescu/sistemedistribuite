/*
Nume: Perescu Antonela Madalina
Grupa: 452
Tema 1
*/

package main

import "net"
import "fmt"
import "bufio"
import "os"

func main() {

	//connect to this socket
	conn, _ := net.Dial("tcp", "127.0.0.1:8080")
	for {
		//read in input from stdin
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Text to send: ")
		text, _ := reader.ReadString('\n')
		//send to socket
		fmt.Fprintf(conn, text + "\n")
		fmt.Printf("Client %s a facut request cu: %s", conn.RemoteAddr(), text)
		//listen for reply
		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Printf("Client %s a primit raspunsul %s: ", conn.RemoteAddr(), message)
	}
}